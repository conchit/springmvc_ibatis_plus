package ${packName}.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ${packName}.model.${className};
import ${packName}.service.${className}Service;

/**
 * ${className}Controller
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@RequestMapping("/${className?uncap_first}")
public class ${className}Controller
{
    @Autowired
    ${className}Service ${className?uncap_first}Service;
    
    public ${className}Controller()
    {
        super();
    }
    
    @RequestMapping(value = "/start")
    @ResponseBody
    public List<${className}> start()
        throws Exception
    {
        return ${className?uncap_first}Service.queryAll();
    }
    
}